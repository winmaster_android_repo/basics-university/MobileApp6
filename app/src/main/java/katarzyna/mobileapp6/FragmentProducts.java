package katarzyna.mobileapp6;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentProducts extends Fragment
{

    View getView;
    String [] spinnerProducts={"warzywa","owoce","nabiał", "mięso"};
    EditText productName;
    DatePicker dataPicker;
    String type;

    public FragmentProducts()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_products, container, false);
        Spinner spinner = (Spinner)view.findViewById(R.id.spinnerProduct);
        productName=(EditText)view.findViewById(R.id.productName);
        dataPicker=(DatePicker) view.findViewById(R.id.datePicker);
        Calendar c = Calendar.getInstance();
        Date date= new Date();
        dataPicker.setMinDate(date.getDate());

        if(spinner!=null)
        {
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                {
                    type = spinnerProducts[position];
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent)
                {

                }
            });
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item, spinnerProducts);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        }
        getView = view;
        return view;
    }

    public void onAttach(Context context)
    {
        super.onAttach(context);
    }

    public void saveInfoAboutProduct()
    {
        String[] data = new String[4];
        data[0] = productName.getText().toString();
        StoringData.addProductTitle(productName.getText().toString());
        String date= String.valueOf(dataPicker.getDayOfMonth())+"."+String.valueOf(dataPicker.getMonth())+"."+String.valueOf(dataPicker.getYear());
        data[1] = date;
        RadioButton radioButton = (RadioButton)getView.findViewById(R.id.radioButtonYes);

        if(radioButton.isChecked())
        {
            data[2] = "Produkt ma gluten";
        }
        else
        {
            data[2] = "Produkt bezglutenowy";
        }

        if(type!=null)
        {
            data[3] = type;
        }
        else
        {
            data[3] = "jedzenie";
        }
        StoringData.addProductData(data);

        Toast.makeText(getContext(),"Zapisano", Toast.LENGTH_SHORT).show();
    }

    void clearInfoProduct()
    {
        productName.setText("");
    }
}
