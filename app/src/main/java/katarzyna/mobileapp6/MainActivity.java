package katarzyna.mobileapp6;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements ActionBar.TabListener, FragmentSwitch.onSelect
{
    FragmentProducts fragmentProducts;
    FragmentSpices fragmentSpices;
    FragmentsProductsList fragmentsProductsList;
    FragmentSpicesList fragmentSpicesList;
    FragmentDeveloper fragmentDeveloper;
    FragmentSwitch fragmentSwitch;
    FragmentTransaction transaction;
    ActionBar.Tab tabDeveloper;
    ActionBar.Tab tabAdd;
    ActionBar.Tab tabListProducts;
    ActionBar.Tab tabListSpieces;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentProducts= new FragmentProducts();
        fragmentSpices = new FragmentSpices();
        fragmentsProductsList= new FragmentsProductsList();
        fragmentSpicesList= new FragmentSpicesList();
        fragmentDeveloper= new FragmentDeveloper();
        fragmentSwitch=new FragmentSwitch();
        transaction = getSupportFragmentManager().beginTransaction();

        transaction.add(R.id.frameSwitch,fragmentSwitch);
        transaction.detach(fragmentSwitch);
        transaction.add(R.id.frameFragments,fragmentProducts);
        transaction.detach(fragmentProducts);
        transaction.add(R.id.frameFragments,fragmentSpices);
        transaction.detach(fragmentSpices);
        transaction.add(R.id.frameFragments,fragmentsProductsList);
        transaction.detach(fragmentsProductsList);
        transaction.add(R.id.frameFragments,fragmentSpicesList);
        transaction.detach(fragmentSpicesList);
        transaction.add(R.id.frameFragments,fragmentDeveloper);
        transaction.detach(fragmentDeveloper);
        transaction.commit();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(actionBar.NAVIGATION_MODE_TABS);

        tabDeveloper =  actionBar.newTab().setTabListener(this);
        tabDeveloper.setIcon(R.drawable.info);
        actionBar.addTab(tabDeveloper);
        tabAdd =  actionBar.newTab().setTabListener(this);
        tabAdd.setIcon(R.drawable.add);
        actionBar.addTab(tabAdd);
        tabListProducts =  actionBar.newTab().setTabListener(this);
        tabListProducts.setIcon(R.drawable.product1);
        actionBar.addTab(tabListProducts);
        tabListSpieces =  actionBar.newTab().setTabListener(this);
        tabListSpieces.setIcon(R.drawable.spices);
        actionBar.addTab(tabListSpieces);
        actionBar.show();

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft)
    {
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.detach(fragmentSwitch);
        transaction.detach(fragmentsProductsList);
        transaction.detach(fragmentSpicesList);
        transaction.detach(fragmentDeveloper);
        transaction.detach(fragmentProducts);
        transaction.detach(fragmentSpices);

        if(tab == tabDeveloper)
        {
            transaction.attach(fragmentDeveloper);
        }
        else
        {
            if(tab == tabAdd)
            {
                transaction.attach(fragmentSwitch);
            }
            else
            {
                if(tab == tabListProducts)
                {
                    transaction.attach(fragmentsProductsList);
                }
                else
                {
                    transaction.attach(fragmentSpicesList);
                }
            }
        }
        transaction.commit();
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft)
    {
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.detach(fragmentProducts);
        transaction.detach(fragmentSpices);
        transaction.detach(fragmentSwitch);
        transaction.detach(fragmentsProductsList);
        transaction.detach(fragmentSpicesList);
        transaction.detach(fragmentDeveloper);
        transaction.commit();
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft)
    {

    }

    @Override
    public void onOptionSelect(int option)
    {
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.detach(fragmentProducts);
        transaction.detach(fragmentSpices);
        switch (option)
        {
            case 1:
                transaction.attach(fragmentProducts);
                break;
            case 2:
                transaction.attach(fragmentSpices);
                break;
        }
        transaction.commit();
    }

    public void saveInfoAboutProduct(View view)
    {
        fragmentProducts.saveInfoAboutProduct();
    }
    public void clearInfoProduct(View view)
    {
        fragmentProducts.clearInfoProduct();
    }

    public void saveInfoAboutSpice(View view)
    {
        fragmentSpices.saveInfoAboutSpice();
    }
    public void clearInfoSpice(View view)
    {
        fragmentSpices.clearInfoSpice();
    }
}
