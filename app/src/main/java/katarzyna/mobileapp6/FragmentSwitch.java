package katarzyna.mobileapp6;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSwitch extends Fragment implements RadioGroup.OnCheckedChangeListener
{
    AppCompatActivity appCompatActivity;
    onSelect selected;


    public FragmentSwitch()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_switch, container, false);
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState )
    {
        super.onActivityCreated(savedInstanceState);
        ((RadioGroup)getActivity().findViewById(R.id.radioGroup)).setOnCheckedChangeListener(this);
    }

    public void onAttach(Context context)
    {
        super.onAttach(context);
        try
        {
            appCompatActivity = (AppCompatActivity)context;
            selected = (onSelect)context;
        }
        catch (ClassCastException e)
        {
            Toast.makeText(context,"Something went wrong",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int id)
    {
        switch (id)
        {
            case R.id.radioButtonProducts:
                selected.onOptionSelect(1);
                break;
            case R.id.radioButtonSpices:;
                selected.onOptionSelect(2);
                break;
        }

    }


    public interface onSelect
    {
        public void onOptionSelect(int option);
    }

}
