package katarzyna.mobileapp6;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentsProductsList extends Fragment
{
    ListView listOfProducts;

    public FragmentsProductsList()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragments_products_list, container, false);
        listOfProducts = (ListView)v.findViewById(R.id.listViewProducts);

        listOfProducts.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                String[] properties = StoringData.productsPropertyList.get(position);
                Toast.makeText(getContext(),"Data ważności:  "+properties[1]+", "+properties[2]+", Rodzaj: "+properties[3],Toast.LENGTH_SHORT).show();
            }
        });

        MyAdapter adapter = new MyAdapter();
        listOfProducts.setAdapter(adapter);
        return v;
    }

    public class MyAdapter extends BaseAdapter
    {

        @Override
        public int getCount()
        {
            return StoringData.productsTitleList.size();
        }

        @Override
        public Object getItem(int i)
        {
            return i;
        }

        @Override
        public long getItemId(int i)
        {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup)
        {
            View v = view;

            if (v == null)
            {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                v = vi.inflate(R.layout.list_row_prod, null);
            }

            TextView tv1 = (TextView) v.findViewById(R.id.row_tv1);
            tv1.setText(StoringData.productsTitleList.get(i));
            return v;
        }
    }
}
