package katarzyna.mobileapp6;

import java.util.ArrayList;

/**
 * Created by Katarzyna on 2017-05-17.
 */

public class StoringData
{
    public static ArrayList<String> productsTitleList = new ArrayList<>();
    public static ArrayList<String> spicesTitleList = new ArrayList<>();
    public static ArrayList<String[]> productsPropertyList = new ArrayList<>();
    public static ArrayList<String[]> spicesPropertyList = new ArrayList<>();

    public static void addProductTitle(String title)
    {
        productsTitleList.add(title);
    }

    public static void addSpiceTitle(String title)
    {
        spicesTitleList.add(title);
    }

    public static void addProductData(String data[])
    {
        productsPropertyList.add(data);
    }

    public  static void addSpiceData(String data[])
    {
        spicesPropertyList.add(data);
    }
}
