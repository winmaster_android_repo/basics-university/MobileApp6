package katarzyna.mobileapp6;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSpices extends Fragment
{
    View getView;
    String [] spinnerSpices={"ostra","słodka","gorzka", "ziołowa"};
    EditText spiceName;
    EditText spiceAmount;
    String type;

    public FragmentSpices()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_spices, container, false);
        Spinner spinner = (Spinner)view.findViewById(R.id.spiceSpinner);
        spiceName=(EditText)view.findViewById(R.id.spiceName);
        spiceAmount=(EditText)view.findViewById(R.id.spiceAmount);

        if(spinner!=null)
        {
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                {
                    type = spinnerSpices[position];
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent)
                {

                }
            });
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item, spinnerSpices);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        }
        getView = view;
        return view;
    }

    public void onAttach(Context context)
    {
        super.onAttach(context);
    }


    void saveInfoAboutSpice()
    {
        String[] data = new String[4];
        data[0] = spiceName.getText().toString();
        StoringData.addSpiceTitle(spiceName.getText().toString());
        data[1] = spiceAmount.getText().toString();

        RadioButton radioButton = (RadioButton)getView.findViewById(R.id.radioButtonTrue);

        if(radioButton.isChecked())
        {
            data[2] = "Przyprawa to alergen";
        }
        else
        {
            data[2] = "Brak alergenu";
        }

        if(type!=null)
        {
            data[3] = type;
        }
        else
        {
            data[3] = "umami";
        }
        StoringData.addSpiceData(data);

        Toast.makeText(getContext(),"Zapisano", Toast.LENGTH_SHORT).show();

    }

    void clearInfoSpice()
    {
        spiceAmount.setText("");
        spiceName.setText("");
    }
}
