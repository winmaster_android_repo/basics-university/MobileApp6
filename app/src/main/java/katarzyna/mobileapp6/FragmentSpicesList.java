package katarzyna.mobileapp6;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSpicesList extends Fragment
{
    ListView listOfSpices;

    public FragmentSpicesList()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_spices_list, container, false);
        listOfSpices = (ListView)v.findViewById(R.id.listViewSpices);

        listOfSpices.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                String[] properties = StoringData.spicesPropertyList.get(position);
                Toast.makeText(getContext(),"Ilość w gramach:  "+properties[1]+", "+properties[2]+", Rodzaj: "+properties[3],Toast.LENGTH_SHORT).show();
            }
        });

        MyAdapter adapter = new MyAdapter();
        listOfSpices.setAdapter(adapter);
        return v;
    }


    public class MyAdapter extends BaseAdapter
    {

        @Override
        public int getCount()
        {
            return StoringData.spicesTitleList.size();
        }

        @Override
        public Object getItem(int i)
        {
            return i;
        }

        @Override
        public long getItemId(int i)
        {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup)
        {
            View v = view;

            if (v == null)
            {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                v = vi.inflate(R.layout.list_row_spice, null);
            }

            TextView tv1 = (TextView) v.findViewById(R.id.row_tv1);
            tv1.setText(StoringData.spicesTitleList.get(i));
            return v;
        }


    }

}
